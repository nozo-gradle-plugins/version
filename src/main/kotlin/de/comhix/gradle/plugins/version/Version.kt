@file:Suppress("unused")

package de.comhix.gradle.plugins.version

const val BUILD_NUMBER_ENV_VAR = "CI_PIPELINE_IID"

data class Version(
    val major: Int,
    val minor: Int,
    val build: Int? = System.getenv(BUILD_NUMBER_ENV_VAR)?.toIntOrNull(),
    val snapshotPostfix: String = "-SNAPSHOT"
) {
    override fun toString(): String {
        return if (build == null) {
            format("{major}.{minor}$snapshotPostfix")
        }
        else {
            format("{major}.{minor}.{build}")
        }
    }

    fun format(format: String): String {
        return format.replace("{major}", major.toString())
            .replace("{minor}", minor.toString())
            .replace("{build}", build?.toString() ?: "")
    }

    val isSnapshot = build == null
}