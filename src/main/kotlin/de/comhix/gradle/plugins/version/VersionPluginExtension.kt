@file:Suppress("LeakingThis")

package de.comhix.gradle.plugins.version

import org.gradle.api.provider.Property

abstract class VersionPluginExtension {
    abstract val envVarName: Property<String>
    abstract val envPrefix: Property<String>
    abstract val versionFormat: Property<String>

    init {
        envVarName.convention("VERSION")
        envPrefix.convention("v")
        versionFormat.convention("{major}.{minor}.{build}")
    }
}