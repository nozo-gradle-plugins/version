# Version Plugin

Adds some utilities regarding the project version. Usable to automatically add Build Number to the Version String or create files containing the
version.

## Usage

### Plugin

``` build.gradle.kts
plugins {
    id("de.comhix.gradle.plugins.version") version "1.0.29"
}
```

### Version

``` build.gradle.kts
import de.comhix.gradle.plugins.version.Version

version = Version(1, 0)
```

### Configuration

``` build.gradle.kts
version{
    envVarName.set("FOO") // default "VERSION"
    envPrefix.set("version-") // default "v"
}
```

#### Todos
- move task specific variables to task configuration
- create config entry for build number env variable

### Tasks

#### versionBadge
Creates a badge from `https://img.shields.io/badge/Version-$version-blue` like ![](https://img.shields.io/badge/Version-1.0.29-blue)

#### versionEnv
Creates a text file to use for environment variables, e.g. for gitlab release job

```
FOO=version-1.0.29
```

#### versionInfo
Writes the version into a text file without any extras
```
1.0.29
```

#### Todos
- config value to format version string instead of env variable name/prefix
- replace `versionEnv` and `versionInfo` with one task using the format string
- configurable badge url