import org.gradle.api.JavaVersion.VERSION_17

plugins {
    kotlin("jvm") version buildVersions.PluginVersion.kotlin
    id("java-gradle-plugin")
    id("jacoco")
    id("maven-publish")
    id("signing")
    id("org.jetbrains.dokka") version buildVersions.PluginVersion.dokka
    id("de.undercouch.download") version buildVersions.PluginVersion.download
    id("com.gradle.plugin-publish") version buildVersions.PluginVersion.pluginPublish
}

val javaVersion = VERSION_17
jacoco.toolVersion = buildVersions.PluginVersion.jacoco

group = "de.comhix.gradle.plugins"
version = Version(1, 5)

repositories {
    mavenCentral()
}

dependencies {
    implementation("de.undercouch:gradle-download-task:${buildVersions.PluginVersion.download}")
    testImplementation(kotlin("test"))
    testImplementation(kotlin("test-testng"))
    testImplementation("org.amshove.kluent:kluent:${buildVersions.DependencyVersions.kluentVersion}")
}

gradlePlugin {
    plugins {
        create(name) {
            displayName = "Version Plugin"
            description = "Plugin to create e.g. a version badge or an environment file for gitlab CI"
            id = "$group.$name"
            implementationClass = "$group.$name.VersionPlugin"
            tags.set(listOf("version", "gitlab", "badge"))
        }
    }
    website.set("https://gitlab.com/nozo-gradle-plugins/version")
    vcsUrl.set("https://gitlab.com/nozo-gradle-plugins/version.git")

}

tasks.compileKotlin {
    kotlinOptions.jvmTarget = javaVersion.toString()
}

java {
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
}

tasks.test {
    useTestNG()
}

val dokkaJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
        xml.outputLocation.set(file("$buildDir/jacoco/jacoco.xml"))
        csv.required.set(false)
        html.outputLocation.set(file("${buildDir}/reports/jacocoHtml"))
    }
}

tasks.check {
    dependsOn(tasks.jacocoTestReport)
}

publishing {
    publications {
        create<MavenPublication>("default") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("$buildDir/repository")
        }
    }
}

signing {
    isRequired = !project.version.toString().endsWith("SNAPSHOT")
}

tasks.register("versionEnv", DefaultTask::class.java) {
    group = "version"
    val file = File("${project.buildDir}/version.env")
    outputs.file(file)

    val version = project.version as Version
    actions = listOf(
        Action { file.writeText("VERSION=v${version.major}.${version.minor}") }
    )
}

tasks.register("versionBadge", de.undercouch.gradle.tasks.download.Download::class.java) {
    group = "version"
    val version = project.version.toString().replace("-", ".")
    src("https://img.shields.io/badge/Version-$version-blue")
    dest("${project.buildDir}/version.svg")
}

data class Version(val major: Int, val minor: Int, val build: Int? = System.getenv("CI_PIPELINE_IID")?.toIntOrNull()) {
    override fun toString(): String {
        return "$major.$minor.${build ?: "0-SNAPSHOT"}"
    }

    val isSnapshot = build == null
}