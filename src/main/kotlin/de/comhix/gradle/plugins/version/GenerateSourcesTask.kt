package de.comhix.gradle.plugins.version

import org.gradle.api.DefaultTask
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.*
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

abstract class GenerateSourcesTask : DefaultTask() {
    @get:OutputDirectory
    abstract val outputDir: DirectoryProperty

    @get:Input
    abstract val packageName: Property<String>

    @get:Input
    abstract val fileName: Property<String>

    @get:Input
    abstract val fileContent: Property<String>

    @TaskAction
    fun generateSources() {
        val outputFile = File(
            "${outputDir.get()}/${
                packageName.get().replace(
                    ".",
                    "/"
                )
            }"
        )
        val outputDir = outputFile.parentFile

        if (!outputDir.exists()) outputDir.mkdirs()

        Files.write(
            Paths.get(
                outputDir.absolutePath,
                fileName.get()
            ),
            """
                |package $packageName
                |
                |$fileContent
                |
            """.trimMargin().toByteArray()
        )
    }
}