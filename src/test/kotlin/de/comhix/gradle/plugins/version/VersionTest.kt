package de.comhix.gradle.plugins.version

import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be null`
import kotlin.test.Test

/**
 * @author Benjamin Beeker
 */
class VersionTest {

    @Test
    fun `test snapshot version`() {

        // when
        val version = Version(3, 4, null)

        // then
        version.major `should be equal to` 3
        version.minor `should be equal to` 4
        version.build.`should be null`()
        version.toString() `should be equal to` "3.4-SNAPSHOT"
    }

    @Test
    fun `test build version`() {

        // when
        val version = Version(3, 4, 666)

        // then
        version.major `should be equal to` 3
        version.minor `should be equal to` 4
        version.build `should be equal to` 666
        version.toString() `should be equal to` "3.4.666"
    }
}