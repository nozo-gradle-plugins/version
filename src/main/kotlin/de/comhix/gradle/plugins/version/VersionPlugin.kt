@file:Suppress("unused")

package de.comhix.gradle.plugins.version

import de.undercouch.gradle.tasks.download.Download
import org.gradle.api.*
import java.io.File

class VersionPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        val extension = target.extensions.create("version", VersionPluginExtension::class.java)

        target.tasks.register("versionInfo", DefaultTask::class.java) {
            it.group = "version"

            val file = File("${target.buildDir}/version.info")
            it.outputs.file(file)

            it.actions = listOf(
                Action { file.writeText(target.version.toVersionString(extension)) }
            )
        }

        target.tasks.register("versionEnv", DefaultTask::class.java) {
            it.group = "version"

            val file = File("${target.buildDir}/version.env")
            it.outputs.file(file)

            it.actions = listOf(
                Action { file.writeText("${extension.envVarName.get()}=${extension.envPrefix.get()}${target.version.toVersionString(extension)}") }
            )
        }

        target.tasks.register("versionBadge", Download::class.java) {
            it.group = "version"

            val version = target.version.toVersionString(extension).replace("-", ".")

            it.src("https://img.shields.io/badge/Version-$version-blue")
            it.dest("${target.buildDir}/version.svg")
        }
    }

    private fun Any.toVersionString(extension: VersionPluginExtension): String {
        return if (this is Version) {
            this.format(extension.versionFormat.get())
        }
        else {
            this.toString()
        }
    }
}